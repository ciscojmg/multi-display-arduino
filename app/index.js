// server
const express = require('express');
const http = require('http');

// serial port
const SerialPort = require('serialport');
const ByteLength = require('@serialport/parser-byte-length');
const Delimiter = require('@serialport/parser-delimiter');

// socketio
const SocketIo = require('socket.io');


const app = express();
const server = http.createServer(app);
const io = SocketIo.listen(server);


// routes
app.get('/', (req, res) => {
    res.sendFile(__dirname +'/index.html');
});


// create serial port
const port = new SerialPort('COM30', {
    baudRate: 9600
});

//option serial port
// const parser = port.pipe(new ByteLength({length: 5}));
const parser = port.pipe(new Delimiter({ delimiter: '\n' }));

//open serial port
port.on('open', function () {
    console.log('Opened Port.');
});

//read data serial port
parser.on('data', function (data) {
    // console.log(data.toString());
    let dataArray = data.toString().trim().split(',');
    let rssi = dataArray[0].toString();
    let grams = dataArray[1].toString();
    let weight = dataArray[2].toString();
    io.emit('arduino:rssi', {
        rssi: rssi, 
        grams: grams,
        weight: weight

    });
});

//error serial port
port.on('err', function (data) {
    console.log(err.message);
});

// listen server
server.listen(3000, () => {
    console.log('Server on port 3000');
});



