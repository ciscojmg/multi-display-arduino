# multidisplay-arduino

Gracifica con arduino

* arduino
* node.js
  * socket.io
  * serialport
  * express

Para ejecutar el proyecto se debe tomar en consideracion el puerto serial por el cual se conecta el **arduino**, en este caso es el **COM3**. Modificar esa parte del codigo si cambia.

### Dependencias

Debe tener instalado [node.js](https://nodejs.org/en/)

* Entrar a la carpeta app `cd app/`
* Instalar dependencia `npm install`
* Ejecutar proyecto `node index.js`
* Ingresar a [localhost](http://localhost:3000/)

#### Multimedia
![line](/uploads/94b93f955fd984dbe21e2489fc257aaa/line.gif)
